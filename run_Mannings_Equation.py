#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 13:55:14 2020

@author: Aidans
"""

import Mannings_Equation as meq
import numpy as np
import matplotlib.pyplot as plt

s0 = .025
width = 2
n = .030 #.025 to .033 strainght channel
channel = meq.ManningsEquation(s0, width, n)

times = np.array([0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6])
discharge = np.array([0.1, 0.2, 0.5, 1, 1.75, 1.7, 1.6, 1.4, 1.2, 1, 0.8, 0.6])

depth0 = 2. #m

### Effects of Changing wetted perimeter

widths = np.linspace(1, 3, 5)
plt.figure()

for i in widths:
    channel.width = i
    depth, error, flow_vel = channel.NR_depth_solver(depth0, discharge)
    channel.reset()
    plt.xticks(size = 20)
    plt.yticks(size = 20)
    plt.title('Flow velocity Sensitivity to Channel Width', fontsize = 30)
    plt.ylabel('Flow Velocity (m/s)', fontsize = 20)
    plt.xlabel('Time (hrs)', fontsize = 20)
    plt.plot(times, flow_vel,label = 'Channel width = ' + str(i) + 'm', linewidth = 2)
    plt.grid()
    plt.legend()


### Relationship between flow depth and discharge
discharge2 = np.linspace(.1, 2., 20)
channel.reset()
channel.width = 2. 
depth, error, flow_vel = channel.NR_depth_solver(depth0, discharge2)

plt.figure()
plt.xticks(size = 20)
plt.yticks(size = 20)
plt.title('Flow Depth vs Discharge', fontsize = 30)
plt.ylabel('Flow Depth (m)', fontsize = 20)
plt.xlabel('Discharge (m^3/s)', fontsize = 20)
plt.plot(depth, discharge2, '.', markersize = 15)
plt.grid()


### Effect of increased vegetation density on flow depth

channel.reset()
depth, error, flow_vel = channel.NR_depth_solver(depth0, discharge)

plt.figure()
plt.xticks(size = 20)
plt.yticks(size = 20)
plt.title('Flow Depth in a weedy and not weedy reach', fontsize = 30)
plt.ylabel('Flow Depth (m)', fontsize = 20)
plt.xlabel('Time (hrs)', fontsize = 20)
plt.plot(times, depth, label = 'Not Weedy', linewidth = 2)
plt.grid()

channel.reset()
channel.n = .070
depth, error, flow_vel = channel.NR_depth_solver(depth0, discharge)
plt.plot(times, depth, label = 'Weedy', linewidth = 2)

channel.reset()
channel.n = .1
depth, error, flow_vel = channel.NR_depth_solver(depth0, discharge)
plt.plot(times, depth, label = 'Very Weedy', linewidth = 2)
plt.legend()

### What is the uncertainty in the calculated flow depth for the coice of mannings roughness?

plt.figure()
plt.xticks(size = 20)
plt.yticks(size = 20)
plt.title('Flow Depth Uncertainty wrt Mannings Coefficient', fontsize = 30)
plt.ylabel('Flow Depth (m)', fontsize = 20)
plt.xlabel('Time (hrs)', fontsize = 20)
plt.grid()

channel.reset()
channel.n = .033
depth, error, flow_vel = channel.NR_depth_solver(depth0, discharge)
plt.plot(times, depth, label = 'Max n=.033', color = 'blue', linewidth = 2)
for i in range(20):
    channel.reset()
    channel.n -= .008/20
    depth, error, flow_vel = channel.NR_depth_solver(depth0, discharge)
    plt.plot(times, depth, color = 'blue', linewidth = 2, alpha = .1)


channel.reset()
channel.n = .025
depth, error, flow_vel = channel.NR_depth_solver(depth0, discharge)
plt.plot(times, depth, 'b--', label = 'Min n=.025', linewidth = 2)
plt.legend()

 