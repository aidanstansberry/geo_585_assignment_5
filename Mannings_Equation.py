#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 10:00:21 2020

@author: Aidans
"""

import numpy as np

class ManningsEquation():
    
    def __init__(self, s0, width, n):
        """
        Parameters
        ----------
        s0 : channel slope (dimensionless)
        
        depth : depth of the water in the channel (m)
        
        width : width of the cannel (m)
        
        n : Mannings roughness coefficient (s/m^(1/3))
        
        Returns
        -------
        None."""
        
        self.s0 = s0
        
        self.width = width
        
        self.n = n
        
        self.depths = []
        
        self.flowvelocities = []
        
        self.errors = []
    
    def _area(self, depth):
        
        """
        Solves for the cross sectional area of a rectangular channel. 
           
        Parameters
        ----------   
        
        depth : depth of the water in the channel (m)
    
        Returns
        -------
        Cross sectional areal of the channel in m^2"""
        
        return self.width*depth
    
    def _darea(self, depth):
        
        """
        Solves for the derivative of the cross sectional area with respect to depth
           
        Parameters
        ----------   
        
        depth : depth of the water in the channel (m)
    
        Returns
        -------
        derivative of area in m"""
        
        return self.width
    
    def _perimeter(self, depth):
        
        """
        Solves for the perimeter of a rectangular channel. 
           
        Parameters
        ----------   
        
        depth : depth of the water in the channel (m)
    
        Returns
        -------
        Perimeter length of the channel in m"""
        
        return 2*depth + self.width
    
    def _dperimeter(self, depth):
        
        """
        Solves for the derivative of the perimeter of a rectangular 
        channel with respect to the depth. 
           
        Parameters
        ----------   
        
        depth : depth of the water in the channel (m)
    
        Returns
        -------
        derivative of perimeter length of the channel (dimensionless)"""
        
        return 2.
    
    def _R(self, depth):
        
        """
        Solves for the hydraulic radius of the channel
           
        Parameters
        ----------   
    
        depth : depth of the water in the channel (m)
    
        Returns
        -------
        Hydraulic Radius in m"""
        
        return self._area(depth)/self._perimeter(depth)
    
    def _dR(self, depth):
        
        """
        Solves for the derivative of the hydraulic radius wrt depth
           
        Parameters
        ----------   
    
        depth : depth of the water in the channel (m)
    
        Returns
        -------
        derivative of Hydraulic Radius (dimensionless)"""
        
        return self._darea(depth)/self._perimeter(depth) - self._area(depth)*self._dperimeter(depth)/(self._perimeter(depth))**2
    
    def _flowv(self, depth, Q):
        
        """
        Solves for the flow velocity in the channel
           
        Parameters
        ----------   
    
        depth : depth of the water in the channel (m)
        
        Q : discharge of the channel (m^3s^-1)
    
        Returns
        -------
        Flow Velocity in m/s"""
        
        return Q/self._area(depth)
    
    def _error(self, depth, Q):
        
        
        """
        Produces the error given a discharge and depth 
           
        Parameters
        ----------   
    
        depth : depth of the water in the channel (m)
        
        Q : discharge of the channel (m^3s^-1)
    
        Returns
        -------
        Flow Velocity error in m^3/s"""
        
        return Q - self._area(depth)*(self._R(depth))**(2/3)*np.sqrt(self.s0)/self.n
        
    def _derror(self, depth, Q):
        
        
        """
        Produces the change in error given a discharge and depth wrt depth
           
        Parameters
        ----------   
    
        depth : depth of the water in the channel (m)
        
        Q : discharge of the channel (m^3s^-1)
    
        Returns
        -------
        Flow Velocity error in m^3/s"""
        
        prod_rule_term1 = - self._darea(depth)*(self._R(depth))**(2/3)*np.sqrt(self.s0)/self.n
        prod_rule_term2 = - self._area(depth)*(2/3)*(self._R(depth))**(-1/3)*self._dR(depth)*np.sqrt(self.s0)/self.n
        
        return prod_rule_term1+prod_rule_term2
     
    def NR_depth_solver(self, depth_0, Q):
        
        """
        Uses the newton rhapson method to find the water depth for a given discharge
           
        Parameters
        ----------   
    
        depth : depth of the water in the channel (m)
        
        Q : list or array of discharges of the channel (m^3s^-1)
    
        Returns
        -------
        Depth in m, Error, Flow Velocity in m/s"""
        
        depth = depth_0 
        
        for dis in Q:
            
            i = 0
        
            error = self._error(depth, dis)
        
            while abs(error)>1e-6 and i<100:
            
                error = self._error(depth, dis)
                
                derror = self._derror(depth, dis)
            
                depth = depth - error/derror
                
                i+=1
                
            self.flowvelocities.append(self._flowv(depth, dis))
            
            self.depths.append(depth)
            
            self.errors.append(error)
            
        return np.array(self.depths), np.array(self.errors), np.array(self.flowvelocities)

    def reset(self):
        
        """
        resets the lists which contain the depths, errors, and 
        flowvelocities for the time series
    
        Returns
        -------
        nothing"""
        
        self.depths = []
        
        self.flowvelocities = []
        
        self.errors = []
        
                    
            
        
            
        
            
            
        
        
        
        
        
        
    
    
    
    
    
    
    
    
    
    