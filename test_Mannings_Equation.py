#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 13:54:35 2020

@author: Aidans
"""

import Mannings_Equation as meq
import nose
import numpy as np

class TestSurfaceEnergyBalance(object):
    
    @classmethod
    def setup_class(cls):
        """this method is run once for each class before any tests are run"""
        cls.s0 = .0156 #unitless
        cls.width = 2.24 #m
        cls.n = .0314 #dimensionless
        
    @classmethod
    def teardown_class(cls):
        """this method is run once for each class __after__ all tests are run"""
        pass
    
    def setUp(self):
        """this method is run once before __each__ tests is run"""
        pass
    
    def teardown(self):
        """this method is run once after __each__ tests is run"""
        pass

    def test_init(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        nose.tools.assert_equal(channel.s0, self.s0)
        nose.tools.assert_equal(channel.width, self.width)
        nose.tools.assert_equal(channel.n, self.n)


    def test_area(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth = .341
        
        nose.tools.assert_equal(channel._area(depth), self.width*depth)

    def test_darea(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth = .341
        
        nose.tools.assert_equal(channel._darea(depth), self.width)
        
    def test_perimeter(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth = .341
        
        nose.tools.assert_equal(channel._perimeter(depth), self.width+2*depth)
    
    def test_dperimeter(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth = .341
        
        nose.tools.assert_equal(channel._dperimeter(depth), 2.)
        
    def test_R(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth = .341
        
        perimeter = self.width + 2*depth
        
        area = self.width*depth
        
        nose.tools.assert_equal(channel._R(depth), area/perimeter)
            
    def test_dR(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth = .341
        
        dR = self.width**2/(self.width+2*depth)**2 #from wolfram NOT my implementation!
        
        nose.tools.assert_almost_equal(channel._dR(depth), dR, places = 6)
        
    def test_flowv(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth = .341
        
        Q = 4.32
        
        v = Q/(depth*self.width)
        
        nose.tools.assert_equal(channel._flowv(depth, Q), v)
        
    def test_error(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth = .341
        
        Q = 4.32
        
        area = depth*self.width
        
        R = area/(2*depth+self.width)
        
        error = Q - area*(R)**(2/3)*np.sqrt(self.s0)/self.n
        
        nose.tools.assert_equal(channel._error(depth, Q), error)
        
    def test_derror(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth = .341
        
        Q = 4.32
        
        perimeter = self.width+2*depth
        
        area = self.width*depth
        
        dR = self.width/(perimeter) - 2*area/(perimeter)**2
        
        term1 = -2*np.sqrt(self.s0)*self.width*depth*(dR)/(3*self.n*(area/perimeter)**(1/3))
        
        term2 = -np.sqrt(self.s0)*self.width*(area/perimeter)**(2/3)/self.n
        
        nose.tools.assert_almost_equal(channel._derror(depth, Q), term1+term2, places = 6)
        
    def test_NR_depth_solver(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth0 = .341
        
        Q = np.array([4.32, 1.23])
        
        depthf, error, v = channel.NR_depth_solver(depth0, Q)
        
        Qpred = (self.width*depthf/(2*depthf+self.width))**(2/3)*np.sqrt(self.s0)/self.n*self.width*depthf
        
        for i in range(len(Q)):
            nose.tools.assert_almost_equal(Q[i], Qpred[i], places = 6)
        
        Q = [4.32, 1.23] # make sure it also works for a list
        
        depthf, error, v = channel.NR_depth_solver(depth0, Q)
        
        Qpred = (self.width*depthf/(2*depthf+self.width))**(2/3)*np.sqrt(self.s0)/self.n*self.width*depthf
        
        for i in range(len(Q)):
            nose.tools.assert_almost_equal(Q[i], Qpred[i], places = 6)
            
    def test_reset(self):
        
        channel = meq.ManningsEquation(self.s0, self.width, self.n)
        
        depth0 = .341
        
        Q = np.array([4.32, 1.23])
        
        depthf, error, v = channel.NR_depth_solver(depth0, Q)
        
        Qpred = (self.width*depthf/(2*depthf+self.width))**(2/3)*np.sqrt(self.s0)/self.n*self.width*depthf
        
        channel.reset()
        
        nose.tools.assert_equal(channel.depths, [])
        nose.tools.assert_equal(channel.errors, [])
        nose.tools.assert_equal(channel.flowvelocities, [])
            
        
            
            
        
        
    
        
